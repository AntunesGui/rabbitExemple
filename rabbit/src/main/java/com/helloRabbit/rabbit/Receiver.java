package com.helloRabbit.rabbit;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.concurrent.CountDownLatch;
import org.springframework.stereotype.Component;
import java.io.IOException;



@Component
public class Receiver {

    private CountDownLatch latch = new CountDownLatch(1);

    public void receiveMessage(String message) throws IOException {
        System.out.println("Received <" + message + ">");
        latch.countDown();
		BufferedWriter writer = new BufferedWriter(new FileWriter("log.txt", true));
		writer.write(message);
		writer.newLine();
		writer.close();
    }

    public CountDownLatch getLatch() {
        return latch;
    }

}
